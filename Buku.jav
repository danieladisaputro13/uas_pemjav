/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package user.uas_pemjav;

/**
 *
 * @author uyaoy
 */
abstract class Buku {
    protected String judul;
    protected int hargaBeli;
    protected int hargaJual;
    protected int stok;

    public Buku(String judul, int hargaBeli, int hargaJual, int stok) {
        this.judul = judul;
        this.hargaBeli = hargaBeli;
        this.hargaJual = hargaJual;
        this.stok = stok;
    }

    public abstract void tambahStok(int jumlah);
    public abstract void kurangiStok(int jumlah);
    public abstract void tampilkanInfo();
}

