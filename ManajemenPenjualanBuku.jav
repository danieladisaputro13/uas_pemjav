/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Project/Maven2/JavaApp/src/main/java/${packagePath}/${mainClassName}.java to edit this template
 */

package user.uas_pemjav;
import java.util.ArrayList;
import java.util.Scanner;
/**
 *
 * @author uyaoy
 */
public class ManajemenPenjualanBuku {
    private static int modalAwal = 1000000;
    private static int modalBerjalan = modalAwal;
    private static ArrayList<Buku> daftarBuku = new ArrayList<>();

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("**************************************");
        System.out.println("Sistem Penjualan Buku");
        System.out.println("By: <Daniel Adi Saputro>, <19201118>");
        System.out.println("**************************************");
        
        BukuFiksi bukuFiksi1 = new BukuFiksi("Harry Potter and the Philosopher's Stone", 50000, 80000, 50);
        BukuFiksi bukuFiksi2 = new BukuFiksi("To Kill a Mockingbird", 45000, 75000, 30);
        BukuNonFiksi bukuNonFiksi1 = new BukuNonFiksi("Sapiens: A Brief History of Humankind", 60000, 95000, 40);
        BukuNonFiksi bukuNonFiksi2 = new BukuNonFiksi("Educated", 55000, 90000, 20);
        Majalah majalah1 = new Majalah("National Geographic", 20000, 35000, 100, 2021);
        Majalah majalah2 = new Majalah("Time", 25000, 40000, 80, 2021);

        daftarBuku.add(bukuFiksi1);
        daftarBuku.add(bukuFiksi2);
        daftarBuku.add(bukuNonFiksi1);
        daftarBuku.add(bukuNonFiksi2);
        daftarBuku.add(majalah1);
        daftarBuku.add(majalah2);


        int pilihanMenu;
        do {
            tampilkanMenu();
            pilihanMenu = scanner.nextInt();

            switch (pilihanMenu) {
                case 1:
                    beliBuku();
                    break;
                case 2:
                    jualBuku();
                    break;
                case 3:
                    lihatStokBuku();
                    break;
                case 4:
                    lihatLaporanKeuangan();
                    break;
                case 5:
                    System.out.println("Program Selesai");
                    break;
                default:
                    System.out.println("Pilihan menu tidak valid.");
                    break;
            }
        } while (pilihanMenu != 5);
    }

    private static void tampilkanMenu() {
        System.out.println("Silakan pilih menu:");
        System.out.println("1) Beli Buku");
        System.out.println("2) Jual Buku");
        System.out.println("3) Lihat Stok Buku");
        System.out.println("4) Lihat Laporan Keuangan");
        System.out.println("5) Exit");
        System.out.print("\nPilihan Menu: ");
    }

    private static void beliBuku() {
        Scanner scanner = new Scanner(System.in);

        System.out.println("\nDaftar Jenis Buku:");
        System.out.println("1) Buku Fiksi");
        System.out.println("2) Buku Non Fiksi");
        System.out.println("3) Majalah");
        System.out.print("Pilih jenis buku: ");
        int jenisBuku = scanner.nextInt();

        System.out.print("Judul: ");
        String judul = scanner.next();
        System.out.print("Harga Beli: ");
        int hargaBeli = scanner.nextInt();
        System.out.print("Harga Jual: ");
        int hargaJual = scanner.nextInt();
        System.out.print("Stok: ");
        int stok = scanner.nextInt();

        switch (jenisBuku) {
            case 1:
                BukuFiksi bukuFiksi = new BukuFiksi(judul, hargaBeli, hargaJual, stok);
                daftarBuku.add(bukuFiksi);
                modalBerjalan += bukuFiksi.hargaBeli * bukuFiksi.stok;
                break;
            case 2:
                BukuNonFiksi bukuNonFiksi = new BukuNonFiksi(judul, hargaBeli, hargaJual, stok);
                daftarBuku.add(bukuNonFiksi);
                modalBerjalan += bukuNonFiksi.hargaBeli * bukuNonFiksi.stok;
                break;
            case 3:
                System.out.print("Nomor Edisi: ");
                int nomorEdisi = scanner.nextInt();
                Majalah majalah = new Majalah(judul, hargaBeli, hargaJual, stok, nomorEdisi);
                daftarBuku.add(majalah);
                modalBerjalan += majalah.hargaBeli * majalah.stok;
                break;
            default:
                System.out.println("Jenis buku tidak valid.");
                break;
        }

        System.out.println("Buku berhasil dibeli.");
        System.out.println("Modal Berjalan: " + modalBerjalan);
    }

    private static void jualBuku() {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Judul Buku yang dijual: ");
        String judul = scanner.next();
        System.out.print("Jumlah yang dijual: ");
        int jumlah = scanner.nextInt();

        boolean bukuDitemukan = false;
        for (Buku buku : daftarBuku) {
            if (buku.judul.equalsIgnoreCase(judul)) {
                bukuDitemukan = true;
                if (buku.stok >= jumlah) {
                    buku.kurangiStok(jumlah);
                    modalBerjalan += buku.hargaJual * jumlah;
                    System.out.println("Buku berhasil dijual.");
                } else {
                    System.out.println("Stok buku tidak mencukupi.");
                }
                break;
            }
        }

        if (!bukuDitemukan) {
            System.out.println("Buku tidak ditemukan.");
        }

        System.out.println("Modal Berjalan: " + modalBerjalan);
    }

    private static void lihatStokBuku() {
        if (daftarBuku.isEmpty()) {
            System.out.println("Belum ada buku yang tersedia.");
        } else {
            System.out.println("\nStok Buku:");
            for (Buku buku : daftarBuku) {
                buku.tampilkanInfo();
                System.out.println();
            }
        }
    }

    private static void lihatLaporanKeuangan() {
        System.out.println("\nLaporan Keuangan:");
        System.out.println("Modal Awal: " + modalAwal);
        System.out.println("Keuntungan Berjalan: " + (modalBerjalan - modalAwal));
        System.out.println("Modal Berjalan: " + modalBerjalan);
    }
}
