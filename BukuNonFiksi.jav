/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package user.uas_pemjav;

/**
 *
 * @author uyaoy
 */
    class BukuNonFiksi extends Buku {
    public BukuNonFiksi(String judul, int hargaBeli, int hargaJual, int stok) {
        super(judul, hargaBeli, hargaJual, stok);
    }

    @Override
    public void tambahStok(int jumlah) {
        this.stok += jumlah;
    }

    @Override
    public void kurangiStok(int jumlah) {
        this.stok -= jumlah;
        if (this.stok == 0) {
            System.out.println("Stok " + this.judul + " telah mencapai nol.");
        }
    }

    @Override
    public void tampilkanInfo() {
        System.out.println("Judul: " + this.judul);
        System.out.println("Harga Beli: " + this.hargaBeli);
        System.out.println("Harga Jual: " + this.hargaJual);
        System.out.println("Stok: " + this.stok);
    }
}

